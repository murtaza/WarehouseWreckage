# WarehouseWreckage
A game about causing chaos in a warehouse using cannonballs!



![](images/WW.gif)


## How to start the game?
1. Download the Windows directory 
2. Extract contents into a folder
3. Open up the Windows/WarehouseWreckage.exe

## How to play?
Move around with WASD 

Use the mouse to look around and aim

Tap spacebar to shoot but beware you only have 20 cannonballs.


